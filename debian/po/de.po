# Copyright (C) 2013
# This file is distributed under the same license as the Pipelight package.
# Michael Müller <michael@fds-team.de>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: Pipelight\n"
"Report-Msgid-Bugs-To: pipelight@packages.debian.org\n"
"POT-Creation-Date: 2013-08-21 05:21+0200\n"
"PO-Revision-Date: 2013-08-21 20:34+0000\n"
"Last-Translator: Michael Müller <michael@fds-team.de>\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-09-12 13:48+0000\n"
"X-Generator: Launchpad (build 16761)\n"
"Language: de\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Do you want to install Pipelight system-wide?"
msgstr "Möchten Sie Pipelight systemweit installieren?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "There are two ways to install Pipelight:"
msgstr "Es gibt zwei Möglichkeiten Pipelight zu installieren:"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"When choosing a system-wide installation (default in previous versions) this "
"will allow you and other users on this computer to use Pipelight in every "
"installed browser supporting the NPAPI plugin interface."
msgstr ""
"Bei einer systemweiten Installation (Standard in vorherigen Versionen) "
"können Sie und andere Benutzer auf diesem Computer Pipelight in allen "
"Browsern benutzen, welche die NPAPI Pluginschnittstelle unterstützen."

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"When not choosing a system-wide installation you will get all files "
"installed, but still have to configure your browser manually to use "
"Pipelight. For instructions on how to do that please visit: "
"https://answers.launchpad.net/pipelight/+faq/2362"
msgstr ""
"Falls Sie sich gegen eine systemweite Installation entscheiden werden alle "
"Dateien installiert, Sie müssen aber noch manuell ihren Browser "
"konfigurieren um Pipelight nutzen zu können. Wie dies geht erfahren Sie "
"unter: https://answers.launchpad.net/pipelight/+faq/2362"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "If unsure choose a systemwide installation."
msgstr ""
"Fals Sie sich unsicher sind, sollten Sie eine systemweite Installation "
"durchführen."
