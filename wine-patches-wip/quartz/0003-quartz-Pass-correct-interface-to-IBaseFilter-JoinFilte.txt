From 852f8bb5fbf83f919371da55b4996739bf65044d Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Michael=20M=C3=BCller?= <michael@fds-team.de>
Date: Sat, 31 Aug 2013 07:04:05 +0200
Subject: quartz: Pass correct interface to IBaseFilter::JoinFilterGraph in
 function FilterGraph2_AddFilter

---
 dlls/quartz/filtergraph.c |    2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/dlls/quartz/filtergraph.c b/dlls/quartz/filtergraph.c
index 0a0beba..0bfef6d 100644
--- a/dlls/quartz/filtergraph.c
+++ b/dlls/quartz/filtergraph.c
@@ -450,7 +450,7 @@ static HRESULT WINAPI FilterGraph2_AddFilter(IFilterGraph2 *iface, IBaseFilter *
         This->filterCapacity = newCapacity;
     }
 
-    hr = IBaseFilter_JoinFilterGraph(pFilter, (IFilterGraph *)This, wszFilterName);
+    hr = IBaseFilter_JoinFilterGraph(pFilter, (IFilterGraph *)&This->IFilterGraph2_iface, wszFilterName);
 
     if (SUCCEEDED(hr))
     {
-- 
1.7.9.5

