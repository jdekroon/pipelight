From 10aca8ddc418a2fb3f8f01582c7d90d2960cad3d Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Michael=20M=C3=BCller?= <michael@fds-team.de>
Date: Sat, 31 Aug 2013 21:39:05 +0200
Subject: quartz: Return VideoMixingRenderer9 when requesting
 VideoMixingRenderer

---
 dlls/quartz/main.c            |    1 +
 dlls/quartz/quartz_strmif.idl |    7 +++++++
 dlls/quartz/regsvr.c          |   12 ++++++++++++
 include/uuids.h               |    1 +
 4 files changed, 21 insertions(+)

diff --git a/dlls/quartz/main.c b/dlls/quartz/main.c
index 2a4071c..1ee31fc 100644
--- a/dlls/quartz/main.c
+++ b/dlls/quartz/main.c
@@ -76,6 +76,7 @@ static const struct object_creation_info object_creation[] =
     { &CLSID_VideoRenderer, VideoRenderer_create },
     { &CLSID_NullRenderer, NullRenderer_create },
     { &CLSID_VideoMixingRenderer9, VMR9Impl_create },
+    { &CLSID_VideoMixingRenderer, VMR9Impl_create },
     { &CLSID_VideoRendererDefault, VideoRendererDefault_create },
     { &CLSID_DSoundRender, DSoundRender_create },
     { &CLSID_AudioRender, DSoundRender_create },
diff --git a/dlls/quartz/quartz_strmif.idl b/dlls/quartz/quartz_strmif.idl
index ed7abe6..2659302 100644
--- a/dlls/quartz/quartz_strmif.idl
+++ b/dlls/quartz/quartz_strmif.idl
@@ -157,3 +157,10 @@ coclass WAVEParser { interface IBaseFilter; }
     uuid(51b4abf3-748f-4e3b-a276-c828330e926a)
 ]
 coclass VideoMixingRenderer9 { interface IBaseFilter; }
+
+[
+    helpstring("Video Mixing Renderer"),
+    threading(both),
+    uuid(b87beb7b-8d29-423f-ae4d-6582c10175ac)
+]
+coclass VideoMixingRenderer { interface IBaseFilter; }
\ No newline at end of file
diff --git a/dlls/quartz/regsvr.c b/dlls/quartz/regsvr.c
index 340dec4..a91b654 100644
--- a/dlls/quartz/regsvr.c
+++ b/dlls/quartz/regsvr.c
@@ -970,6 +970,18 @@ static struct regsvr_filter const filter_list[] = {
             { 0xFFFFFFFF },
         }
     },
+    {   &CLSID_VideoMixingRenderer,
+        &CLSID_LegacyAmFilterCategory,
+        {'V','i','d','e','o',' ','M','i','x','i','n','g',' ','R','e','n','d','e','r','e','r',0},
+        0x200000,
+        {   {   REG_PINFLAG_B_RENDERER,
+                {   { &MEDIATYPE_Video, &GUID_NULL },
+                    { NULL }
+                },
+            },
+            { 0xFFFFFFFF },
+        }
+    },
     {   &CLSID_DSoundRender,
         &CLSID_LegacyAmFilterCategory,
         {'A','u','d','i','o',' ','R','e','n','d','e','r','e','r',0},
diff --git a/include/uuids.h b/include/uuids.h
index 376cca7..5680cfde0 100644
--- a/include/uuids.h
+++ b/include/uuids.h
@@ -276,5 +276,6 @@ OUR_GUID_ENTRY(CLSID_CameraControlPropertyPage,      0x71f96465, 0x78f3, 0x11d0,
 OUR_GUID_ENTRY(CLSID_AnalogVideoDecoderPropertyPage, 0x71f96466, 0x78f3, 0x11d0, 0xa1, 0x8c, 0x00, 0xa0, 0xc9, 0x11, 0x89, 0x56)
 OUR_GUID_ENTRY(CLSID_VideoStreamConfigPropertyPage,  0x71f96467, 0x78f3, 0x11d0, 0xa1, 0x8c, 0x00, 0xa0, 0xc9, 0x11, 0x89, 0x56)
 OUR_GUID_ENTRY(CLSID_VideoMixingRenderer9,           0x51b4abf3, 0x748f, 0x4e3b, 0xa2, 0x76, 0xc8, 0x28, 0x33, 0x0e, 0x92, 0x6a)
+OUR_GUID_ENTRY(CLSID_VideoMixingRenderer,            0xb87beb7b, 0x8d29, 0x423f, 0xae, 0x4d, 0x65, 0x82, 0xc1, 0x01, 0x75, 0xac)
 
 #undef OUR_GUID_ENTRY
-- 
1.7.9.5

